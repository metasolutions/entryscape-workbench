define([
  'dojo/dom-construct',
  'dojo/dom-style',
  '../List',
  '../../defaults',
  './CollectionItem',
  '../../utils/view',
  'mithril',
], (domConstruct, domStyle, List, defaults, CollectionItem, viewUtil, m) => {
  const getViewParams = (viewParams, collectionEntryId) => {
    const sm = defaults.get('siteManager');
    return viewUtil.constructParams(viewParams, Object.assign({}, sm.getUpcomingOrCurrentParams()),
      { collection: collectionEntryId });
  };

  const CollectionItemContainer = {
    /**
     * Creates and shows the list for a selected collection
     *
     * @param selectedCollection
     * @param listNode
     */
    createList(selectedCollection, listNode, placeholderNode) {
      domStyle.set(placeholderNode, 'display', 'none');
      domStyle.set(listNode, 'display', '');

      const newNode = domConstruct.create('div', null, listNode);

      this.list = new List({
        selectedCollection,
      }, newNode);
      this.list.show();
    },
    getCollectionSize(collectionSizes, collectionId) {
      return collectionSizes.get(collectionId);
    },
    getCollectionLabel(collectionEntry) {
      return defaults.get('rdfutils').getLabel(collectionEntry);
    },
    view(vnode) {
      const {
        collectionIds, collectionEntries, selectedCollection, collectionSizes, viewParams,
        listContainerNode, placeholderNode, bid,
      } = vnode.attrs;

      const sm = defaults.get('siteManager');
      return collectionIds.map((collectionId) => {
        const collection = collectionEntries.get(collectionId);
        let active = false;

        if (collectionId === selectedCollection.getId()) {
          this.createList(collection, listContainerNode, placeholderNode);
          active = true;
        }

        const id = collectionId;
        const size = this.getCollectionSize(collectionSizes, collectionId);
        const title = this.getCollectionLabel(collection);
        const aParams = getViewParams(viewParams, collectionId);
        const href = sm.getViewPath(aParams.view, aParams);

        return m(CollectionItem, { id, title, size, active, href, bid });
      });
    },
  };

  return CollectionItemContainer;
});
