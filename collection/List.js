define([
  'dojo/_base/declare',
  '../defaults',
  'store/types',
  'entryscape-commons/list/EntryRow',
  'entryscape-commons/list/common/BaseList',
  'entryscape-commons/list/common/RemoveDialog',
  './dialogs/RemoveMemberDialog',
  'entryscape-commons/rdforms/RDFormsEditDialog',
  'entryscape-commons/create/typeIndex',
  './dialogs/ManageMembersDialog',
  './dialogs/EditListDialog',
  './dialogs/RemoveListDialog',
  './utils/buttons',
  'i18n!nls/escoList',
  'i18n!nls/eswoCollection',
], (declare, defaults, types, EntryRow, BaseList, RemoveDialog, RemoveMemberDialog,
    RDFormsEditDialog, typeIndex, ManageMembersDialog, EditListDialog, RemoveListDialog,
    buttons) => {
  const SingleButtonEntryRow = declare([EntryRow], {
    showCol1: false,
    showCol3: false,
    rowButtonMenu: false,
  });

  return declare([BaseList], {
    includeCreateButton: false,
    includeEditButton: false,
    includeRemoveButton: false,
    includeRefreshButton: true,
    includeResultSize: false,
    nlsBundles: ['escoList', 'eswoCollection'],
    nlsRemoveEntryConfirm: 'removeEntryFromCollection',
    nlsRemoveCollectionConfirm: 'removeCollectionConfirm',
    nlsDownloadCollection: 'downloadCollection',
    nlsDownloadCollectionTitle: 'downloadCollectionHeaderTitle',
    searchVisibleFromStart: false,
    rowClickDialog: 'edit',
    listActionNames: ['refresh', 'manageMembers', 'editList', 'removeList'],
    listButtonMenu: true,
    rowClass: SingleButtonEntryRow,

    postCreate() {
      // List Actions
      this.registerListAction({
        name: 'manageMembers',
        button: 'success',
        icon: 'plus',
        noMenu: true,
        iconType: 'fa',
        max: this.createLimit,
        disableOnSearch: false,
      });

      this.registerListAction({
        name: 'editList',
        button: 'default',
        icon: 'pencil',
        iconType: 'fa',
        nlsKey: this.nlsEditEntryLabel,
      });

      this.registerListAction({
        name: 'removeList',
        button: 'warning',
        icon: 'remove',
        iconType: 'fa',
        nlsKey: this.nlsRemoveEntryLabel,
      });

      // Register Dialogs
      this.registerDialog('manageMembers', ManageMembersDialog); // add members to collection
      this.registerDialog('delete', RemoveMemberDialog); // remove members from collection
      this.registerDialog('editList', EditListDialog); // edit collection
      this.registerDialog('removeList', RemoveListDialog); // edit collection

      this.inherited(arguments);

      // Row Actions
      this.rowActions = [buttons.delete];
      this.listView.includeResultSize = !!this.includeResultSize; // make this boolean
    },
    show() {
      this.render();
    },
    getSearchObject() {
      const context = defaults.get('context');
      /** @type {store/EntryStore} */
      const es = defaults.get('entrystore');
      return es.newSolrQuery().context(context).lists(this.selectedCollection.getResourceURI());
    },

    getTemplate(entry) {
      const conf = typeIndex.getConf(entry);
      if (conf) {
        return defaults.get('itemstore').getItem(conf.template);
      }

      return defaults.get('itemstore').createTemplateFromChildren([
        'dcterms:title',
        'dcterms:description',
      ]);
    },
  });
});
