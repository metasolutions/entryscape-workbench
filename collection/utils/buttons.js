define([], () => ({
  add: {
    name: 'add',
    icon: 'check',
    button: 'default',
    first: true,
    iconType: 'fa',
  },
  delete: {
    name: 'delete', // 'remove' is a reserved action name
    icon: 'remove',
    button: 'default',
    first: false,
    iconType: 'fa',
  },
}));
