define([
  'dojo/_base/declare',
  'dojo/io-query',
  'dojo/hash',
  'dojo/dom-prop',
  'dojo/query',
  'dojo/dom-attr',
  'dojo/aspect',
  'dojo/dom-construct',
  'dojo/dom-class',
  'dojo/_base/lang',
  'dojo/keys',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'entryscape-commons/create/typeIndex',
  'entryscape-commons/placeholder/Placeholder',
  'entryscape-commons/view/ViewMixin',
  './List',
  'config',
  '../defaults',
  'store/types',
  'dojo/text!./CollectionTemplate.html',
  '../utils/entitytypes',
  '../utils/view',
  'dojo/dom-style',
  'di18n/NLSMixin',
  './components/CollectionItemContainer',
  'i18n!nls/escoList',
  'i18n!nls/eswoCollection',
], (declare, ioQuery, hash, domProp, dQuery, domAttr, aspect, domConstruct, domClass, lang, keys,
    _WidgetBase, _TemplatedMixin, typeIndex, Placeholder, ViewMixin, List, config, defaults, types,
    template, entitytypes, view, domStyle, NLSMixin, CollectionItemContainer) =>

  declare([_WidgetBase, _TemplatedMixin, NLSMixin.Dijit, ViewMixin], {
    bid: 'eswoCollection',
    viewName: 'workbenchcollections',
    specificViewParam: 'collection',
    viewParams: new Set(['view', 'context', 'collection']),
    renderOnCalltype: new Set(['createEntry', 'commitMetadata', 'delEntry', 'addToList', 'removeFromList']),
    nlsBundles: ['eswoCollection'],
    templateString: template,
    __sideList: null,
    __list: null,
    __placeholder: null,
    configuredEntitytypes: [],

    init() {
      this.collections = [];
      this.collectionsEntries = new Map();
      this.collectionSizes = new Map();
      this.allSizesPromises = [];
    },
    postCreate() {
      this.inherited(arguments);
      const sm = defaults.get('siteManager');
      this.init();

      // Register various listeners to handle all collection entry actions
      aspect.after(sm, 'beforeViewChange', lang.hitch(this, (err, params) => {
        if (params[0] !== this.viewName) {
          this.stopListenForCollectionChanges();
          this.init();
          // clear collection view params
          // remove 'collection'
          const viewParams = sm.getUpcomingOrCurrentParams();
          delete viewParams[this.specificViewParam];
        }
      }));

      const es = defaults.get('entrystore');
      es.addAsyncListener(lang.hitch(this, (promise, callType) => {
        if (this.renderOnCalltype.has(callType)) {
          promise.then((entry) => {
            const viewParams = sm.getUpcomingOrCurrentParams();
            if (this.viewName === viewParams.view) {
              let nList;
              let newSize;
              switch (callType) {
                case 'delEntry':
                  this.deleteCollection(this.getCollectionIdFromParams());
                  break;
                case 'createEntry':
                  if (entry.isList()) {
                    this.createCollection(entry);
                  }
                  break;
                case 'addToList':
                  newSize = this.collectionSizes.get(entry.getId()) + 1;
                  this.collectionSizes.set(entry.getId(), newSize);
                  nList = dQuery('.eswoCollection__listItem.active span.badge');
                  domProp.set(nList[0], 'innerHTML', newSize);
                  break;
                case 'removeFromList':
                  newSize = this.collectionSizes.get(entry.getId()) - 1;
                  this.collectionSizes.set(entry.getId(), newSize);
                  nList = dQuery('.eswoCollection__listItem.active span.badge');
                  domProp.set(nList[0], 'innerHTML', newSize);
                  break;
                case 'commitMetadata':
                  if (entry.isList()) {
                    this.updateCollection(entry);
                  }
                  break;
                default:
              }
            }
          });
        }
      }));
    },

    startListenForCollectionChanges() {

    },
    stopListenForCollectionChanges() {

    },

    getCollectionIdFromParams() {
      const st = defaults.get('siteManager');
      // for some reason getUpcomingOrCurrentParams doesn't return the correct value here
      const { collection } = st.getCurrentParams();
      return collection > 0 ? collection : -1;
    },

    deleteCollection(collectionId) {
      this.updateCollectionState(collectionId, null, false, true);
    },
    createCollection(collectionEntry) {
      this.updateCollectionState(collectionEntry.getId(), collectionEntry, true);
    },
    updateCollection(collectionEntry) {
      this.updateCollectionState(collectionEntry.getId(), collectionEntry);
    },
    /**
     * Updates the collection entry in the collection list and triggers a cache refresh.
     *
     * @param collection
     */
    updateCollectionState(collectionId, collection, create = false, remove = false) {
      if (remove) {
        this.collections.splice(this.collections.indexOf(collectionId), 1);
        this.collectionsEntries.delete(collectionId);
        // TODO remove from map as well
      } else if (create) {
        this.collections.push(collectionId);
        this.collectionsEntries.set(collectionId, collection);
        this.collectionSizes.set(collectionId, 0); // set newly created size to 0
      } else {
        this.collectionsEntries.set(collectionId, collection);
      }

      this.validateViewSpecificParam(collectionId);
      this.render(this.getSelectedCollection(collectionId));
    },
    /**
     * Returns the currently selected collection entry or the first collection.
     *
     * @param collectionId
     * @return {V}
     */
    getSelectedCollection(collectionId) {
      if (collectionId && this.collections.indexOf(collectionId) !== -1) {
        return this.collectionsEntries.get(collectionId) ||
          this.collectionsEntries.values().next().value;
      }

      return this.collectionsEntries.values().next().value;
    },

    validateViewSpecificParam(selectedCollectionId) {
      // Append 'collection' param if not present
      // TODO use siteManager for this
      const viewParams = ioQuery.queryToObject(hash());
      if (Object.keys(viewParams).indexOf(this.specificViewParam) === -1) {
        const collectionId = viewParams[this.specificViewParam];
        if (this.collections.indexOf(collectionId) === -1) {
          viewParams[this.specificViewParam] = selectedCollectionId;
          hash(ioQuery.objectToQuery(viewParams));
        }
      } else {
        viewParams[this.specificViewParam] = selectedCollectionId;
        hash(ioQuery.objectToQuery(viewParams));
      }
    },
    /**
     * @param collectionEntry
     * @return Promise
     */
    updateCollectionSize(collectionEntry) {
      return collectionEntry.getResource(true).getEntries().then(() => {
        const collectionId = collectionEntry.getId();
        this.collectionSizes.set(collectionId, collectionEntry.getResource(true).getSize());
      });
    },

    show(params) {
      if (this.collections.length > 0) {
        // do some shit and then render
        const selectedCollection = this.getSelectedCollection(params.collection);
        const selectedCollectionId = selectedCollection.getId();

        this.validateViewSpecificParam(selectedCollectionId);

        this.render(selectedCollection);
      } else {
        const es = defaults.get('entrystore');
        const context = defaults.get('context');

        es.newSolrQuery().context(context).sort('created+desc').graphType(types.GT_LIST)
          .list()
          .forEach((collectionEntry) => {
            this.collections.push(collectionEntry.getId());
            this.collectionsEntries.set(collectionEntry.getId(), collectionEntry);
            this.collectionSizes.set(collectionEntry.getId(), '?');
          })
          .then(() => {
            // Render selected collection
            const selectedCollection = this.getSelectedCollection(params.collection);
            if (selectedCollection) {
              const selectedCollectionId = selectedCollection.getId();

              this.validateViewSpecificParam(selectedCollectionId);

              this.render(selectedCollection);

              // Get sizes for collections asynchronously and re-render
              this.collectionsEntries.forEach((collectionEntry) => {
                this.allSizesPromises.push(this.updateCollectionSize(collectionEntry));
              });

              Promise.all(this.allSizesPromises).then(() => {
                this.render(selectedCollection);
              });
            } else {
              this.render();
            }
          }, this);
      }
    },

    destroyEditors() {
      if (this.list) {
        this.list.destroy();
        delete this.list;
      }
    },

    render(selectedCollection) {
      domStyle.set(this.__multipleEtypes, 'display', 'block');
      domStyle.set(this.__singleEtype, 'display', 'none');
      domAttr.set(this.__list, 'innerHTML', '');
      domStyle.set(this.__placeholder, 'display', '');

      this.destroyEditors();

      if (selectedCollection) {
        m.render(this.__sideList, m(CollectionItemContainer, {
          selectedCollection,
          collectionSizes: this.collectionSizes,
          viewParams: this.viewParams,
          placeholderNode: this.__placeholder,
          collectionIds: this.collections,
          collectionEntries: this.collectionsEntries,
          listContainerNode: this.__list,
          bid: this.bid,
        }));
      } else {
        m.render(this.__sideList, null);
      }
      // todo change the URL's collection to selectedCollection.getId()
    },
    localeChange() {
      if (!this.placeholder) {
        this.placeholder = new Placeholder({}, domConstruct.create('div', null, this.__placeholder));
        this.placeholder.getText = () => this.NLSBundle0.selectEntitytypeMessage;
        this.placeholder.render();
      }
    },
    _checkKey(ev) {
      if (ev.keyCode === keys.ENTER) {
        this._createCollection();
      }
    },
    _createCollection() {
      const defLang = defaults.get('defaultLocale');
      const collectionLabel = this._collectionTitle;
      const label = domAttr.get(collectionLabel, 'value');
      if (label === '' || label == null) {
        return;
      }
      let l;
      if (typeof defLang === 'string' && defLang !== '') {
        l = defLang;
      }

      this._newCollection = defaults.get('context').newList();
      const md = this._newCollection.getMetadata();
      const uri = this._newCollection.getResourceURI();

      md.addL(uri, 'dcterms:title', label, l);

      this._newCollection.setMetadata(md).commit().then(() => {
        // clear collection input field
        domAttr.set(this._collectionTitle, 'value', '');
      });
    },
  }));
