define([
  'dojo/_base/declare',
  'dojo/dom-construct',
  'dojo/dom-style',
  'entryscape-commons/defaults',
  'entryscape-commons/list/common/ListDialogMixin',
  'di18n/NLSMixin',
], (declare, domConstruct, domStyle, defaults, ListDialogMixin, NLSMixin) =>
  declare([ListDialogMixin, NLSMixin.Dijit], {
    nlsBundles: ['eswoCollection'],
    /**
     * This "dialog" does three things:
     * 1. Add a member to a collection
     * 2. refresh the collection list
     * 3. remove button on just added entry row
     *
     * @param params
     */
    open(params) {
      this.inherited(arguments);
      this.selectedCollection = params.list.collectionList;
      this.selectedEntry = params.row.entry;

      // 1. add member to collection
      this.addMember().then(() => {
        // 2. refresh the parent list
        // this.selectedCollectionView refers to the parent collection that initiated the dialog
        // TODO ideally this shouldn't happen here, but the list should be rerendered with the
        // up to date items. However, that is problematic due to Solr indexing taking some time
        // (in the order of sec) to update.
        this.list.selectedCollectionView.addRowForEntry(params.row.entry);

        // 3. change the button to a label
        domStyle.set(params.row.buttonsNode.childNodes[0], 'visibility', 'hidden'); // hack to keep fixed height
        domConstruct.create('span', {
          type: 'button',
          class: 'label label-primary',
          style: 'font-size: 12px',
          innerHTML: defaults.get('localize')(params.list.NLSBundle1.addedMembersLabel), // use
          // nls from parent list
        }, params.row.buttonsNode);
      });
    },
    addMember() {
      return this.selectedCollection.getResource(true).addEntry(this.selectedEntry).then(() => {
        this.selectedCollection.setRefreshNeeded();
        this.selectedCollection.refresh();
      });
    },
  }));
