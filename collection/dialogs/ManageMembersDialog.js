define([
  'dojo/_base/declare',
  'dojo/dom-construct',
  'dojo/dom-class',
  'dojo/dom-style',
  'di18n/NLSMixin',
  'di18n/localize',
  'entryscape-commons/defaults',
  'entryscape-commons/registry',
  'entryscape-commons/dialog/TitleDialog',
  'entryscape-commons/list/common/ListDialogMixin',
  'entryscape-commons/list/EntryRow',
  'entryscape-commons/list/common/BaseList',
  'entryscape-commons/list/common/RemoveDialog',
  'store/types',
  './AddMemberDialog',
  '../components/Note',
  '../utils/buttons',
  'mithril',
], (declare, domConstruct, domClass, domStyle, NLSMixin, localize, defaults, registry,
    TitleDialog, ListDialogMixin, EntryRow, BaseList, RemoveDialog, types, AddMemberDialog,
    Note, buttons, m) => {
  const SingleButtonEntryRow = declare([EntryRow], {
    showCol1: false,
    showCol3: false,
    rowButtonMenu: false,
    postCreate() {
      this.inherited(arguments);
      domStyle.set(this.buttonsNode, 'width', 'initial');
    },

    /**
     * Check if the button should be installed or not depending if the entry is in the
     * collection already
     *
     * @param params
     */
    installButtonOrNot(params) {
      const id = this.entry.getId();
      const ids = this.list.inListEntryIds;

      switch (params.name) {
        case 'add':
          return !ids.has(id);
        default:
          return true;
      }
    },
  });

  const EntryList = declare([BaseList], {
    nlsBundles: ['escoList', 'eswoCollection'],
    includeCreateButton: false,
    rowClass: SingleButtonEntryRow,

    constructor(params) {
      this.collectionList = params.selectedCollection;
      this.collectionResource = this.collectionList.getResource(true);
      this.collectionResource.getAllEntryIds().then((ids) => {
        this.inListEntryIds = new Set(ids);
      });

      this.domNode = params.domNode;
    },

    postCreate() {
      this.inherited('postCreate', arguments);
      this.listView.includeResultSize = !!this.includeResultSize; // make this boolean
      this.listView.domNode = this.domNode;

      this.rowActions = [buttons.add];

      this.registerDialog('add', AddMemberDialog);
    },

    getSearchObject() {
      const context = defaults.get('context');
      /** @type {store/EntryStore} */
      const es = defaults.get('entrystore');

      return es.newSolrQuery().context(context)
        .lists(this.collectionResource.getResourceURI(), true)
        .graphType(types.GT_LIST, true);
    },
  });

  return declare([TitleDialog, ListDialogMixin, NLSMixin.Dijit], {
    maxWidth: 800,
    nlsBundles: ['eswoCollection'],
    nlsHeaderTitle: 'manageMembersHeader',
    nlsFooterButtonLabel: 'manageMembersButton',
    includeNote: true,

    postCreate() {
      this.inherited(arguments);
    },

    localeChange() {
      this.updateLocaleStrings(this.NLSBundle0);
    },
    open(params) {
      this.selectedCollection = params.list.selectedCollection;
      this.selectedCollectionView = params.list.listView;

      this.show();

      this.membersList = new EntryList({
        selectedCollection: this.selectedCollection,
        selectedCollectionView: this.selectedCollectionView,
        domNode: this.containerNode,
      }, domConstruct.create('div', null, this.containerNode));
      this.membersList.render();

      this.inherited(arguments);
    },
    closeNote(e) {
      e.preventDefault();
      domStyle.set(this.noteNode, 'display', 'none');
    },
    renderNote() {
      this.noteNode = domConstruct.create('div', null, this.containerNode);

      const noteText = localize(this.NLSBundle0, 'manageMembersNoteText');
      const addLabelText = localize(this.NLSBundle0, 'addMemberLabel');
      const removeLabelText = localize(this.NLSBundle0, 'removeMemberLabel');

      m.render(this.noteNode, m(Note, {
        text: noteText,
        addLabel: addLabelText,
        removeLabel: removeLabelText,
        onClose: this.closeNote.bind(this),
      }));
    },
    hideComplete() {
      domConstruct.empty(this.containerNode);
    },
  });
});
