define([
  'entryscape-commons/create/typeIndex',
  'entryscape-commons/defaults',
], (typeIndex, defaults) => {
  const sortF = (entityNames, isObj) => {
    const labelAndEntityNames = entityNames.map((entityName) => {
      const conf = isObj ? entityName : typeIndex.getConfByName(entityName);
      return {
        c: conf,
        n: entityName,
        l: defaults.get('localize')(conf.label).toLowerCase(),
      };
    });
    // sort by label
    labelAndEntityNames.sort((a, b) => {
      if (a.l < b.l) {
        return -1;
      } else if (a.l > b.l) {
        return 1;
      }
      return 0; // if names are equal
    });
    if (isObj) {
      return labelAndEntityNames.map(o => o.c);
    }
    return labelAndEntityNames.map(o => o.n);
  };
  const filterEntitytypes = (entityNames, isObj, isTemplate = false) => {
    const filteredEntitytypes = [];
    entityNames.forEach((entityName) => {
      const conf = isObj ? entityName : typeIndex.getConfByName(entityName);
      if (!conf || conf.dependant || (conf.module && conf.module !== 'workbench')) {
        return;
      }
      if (isObj) {
        if (isTemplate) {
          filteredEntitytypes.push(conf.template);
        } else {
          filteredEntitytypes.push(conf);
        }
      } else {
        filteredEntitytypes.push(entityName);
      }
    });
    return filteredEntitytypes;
  };
  return {
    sortNames(names) {
      return sortF(names);
    },
    sort(configurations) {
      return sortF(configurations, true);
    },
    filterEntitypeConfigurations(configurations) {
      return filterEntitytypes(configurations, true);
    },
    filterEntitypeConfigurationsTemplates(configurations) {
      return filterEntitytypes(configurations, true, true);
    },
    filterEntitytypes(names) {
      return filterEntitytypes(names);
    },
    getEntitytypeConfigurationByName(entityName) {
      return typeIndex.getConfByName(entityName);
    },
    getConfFromConstraints(constraintsParams) {
      return typeIndex.getConfFromConstraints(constraintsParams);
    },
  };
});
