define([
  'jquery',
  'dojo/_base/lang',
  'dojo/_base/declare',
  'dojo/promise/all',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dijit/_WidgetsInTemplateMixin',
  'di18n/NLSMixin',
  'store/types',
  'config',
  'di18n/localize',
  'entryscape-commons/overview/components/Overview',
  'entryscape-commons/defaults',
  'entryscape-commons/util/dateUtil',
  'mithril',
  'i18n!nls/eswoOverview',
], (jquery, lang, declare, all, domAttr, domStyle, _WidgetBase, _TemplatedMixin,
    _WidgetsInTemplateMixin, NLSMixin, types, config, localize, Overview, defaults,
    dateUtil, m) =>

  declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, NLSMixin.Dijit], {
    templateString: '<div class="workbenchOverview escoList"></div>',
    nlsBundles: ['eswoOverview'],
    getEntityNameFromURI(entityURI) {
      const es = defaults.get('entrystore');
      if (entityURI.indexOf(es.getBaseURI()) === 0) {
        return entityURI.substr(es.getResourceURI('entitytypes', '').length);
      }
      return entityURI;
    },
    show() {
      this.data = {};
      let projectEntry;
      let configuredEntitytypes;
      const es = defaults.get('entrystore');
      const rdfutils = defaults.get('rdfutils');
      const spa = defaults.get('siteManager');
      const currentOrUpcomingParams = spa.getUpcomingOrCurrentParams();
      const context = defaults.get('context');

      const projectPromise = context.getEntry().then((entry) => {
        projectEntry = entry;
        const ei = entry.getEntryInfo();
        const graph = ei.getGraph();
        configuredEntitytypes = [];
        const entitytypeStmts = graph.find(null, 'esterms:entityType');
        if (entitytypeStmts && entitytypeStmts.length > 0) {
          entitytypeStmts.forEach((entitytype) => {
            configuredEntitytypes.push(this.getEntityNameFromURI(entitytype.getValue()));
          }, this);
        } else {
          config.entitytypes.forEach((configEntitytype) => {
            if (!configEntitytype.module || configEntitytype.module === 'workbench') {
              configuredEntitytypes.push(configEntitytype.name);
            }
          }, this);
        }
      });

      all([this.localeReady, projectPromise]).then(() => {
        const modificationDate = projectEntry.getEntryInfo().getModificationDate();
        const creationDate = projectEntry.getEntryInfo().getCreationDate();
        const modificationDateFormats = dateUtil.getMultipleDateFormats(modificationDate);
        const creationDateFormats = dateUtil.getMultipleDateFormats(creationDate);

        const isNLSBundleReady = this.nlsBundles[0] in this.NLSBundles;
        let b;
        if (isNLSBundleReady) {
          b = this.NLSBundles[this.nlsBundles[0]];
        }

        // basic info
        this.data.description = defaults.get('localize')(rdfutils.getDescription(projectEntry));
        this.data.title = defaults.get('localize')(rdfutils.getLabel(projectEntry));

        // box list
        this.data.bList = [];
        this.data.bList.push({
          key: 'terms',
          label: isNLSBundleReady ? localize(b, 'configuredEntitiesLabel') : 'configuredEntitiesLabel',
          value: configuredEntitytypes.length,
          link: spa.getViewPath('workbench__entities', currentOrUpcomingParams),
        });
        //{
        //  key: 'collections',
        //  label: isNLSBundleReady ? localize(b, 'collectionLabel') : 'collectionLabel',
        //  value: collectionsList.getSize(),
        //  link: spa.getViewPath('workbenchcollections', currentOrUpcomingParams),
        //});

        // stats list
        this.data.sList = [];
        this.data.sList.push(
          {
            key: 'update',
            label: isNLSBundleReady ? localize(b, 'lastUpdatedLabel') : 'lastUpdatedLabel',
            value: modificationDateFormats.short,
          },
          {
            key: 'create',
            label: isNLSBundleReady ? localize(b, 'createdLabel') : 'createdLabel',
            value: creationDateFormats.short,
          });

        m.render(jquery('.workbenchOverview.escoList')[0], m(Overview, { data: this.data }));
      });
    },
  }));
