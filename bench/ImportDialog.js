define([
  'dojo/_base/declare',
  'dojo/_base/array',
  'dojo/_base/lang',
  'dojo/dom-attr',
  'dojo/promise/all',
  'di18n/localize',
  'rdfjson/utils',
  '../defaults',
  'entryscape-commons/dialog/TitleDialog',
  'entryscape-commons/create/EntryType',
  'rdfjson/formats/converters',
  'dojo/text!./ImportDialogTemplate.html',
  'i18n!nls/eswoImportDialog',
], (declare, array, lang, domAttr, all, localize, utils, defaults, TitleDialog, EntryType,
    converters, template) =>

  declare([TitleDialog.ContentNLS], {
    bid: 'eswoImportDialog',
    maxWidth: 800,
    templateString: template,
    nlsBundles: ['eswoImportDialog'],
    nlsHeaderTitle: 'importRDFHeader',
    nlsFooterButtonLabel: 'importRDFButton',
    __entryTypeNode: null,
    __importInformation: null,

    postCreate() {
      this.inherited(arguments);
      this.entryType = new EntryType({
        valueChange: lang.hitch(this, (value) => {
          if (value != null) {
            this.dialog.unlockFooterButton();
          } else {
            this.dialog.lockFooterButton();
          }
        }),
      }, this.__entryTypeNode);
    },
    open() {
      this.entryType.show(true, true, false);
      this.inherited(arguments);
      this.dialog.show();
    },
    localeChange() {
      this.inherited(arguments);
      let rt = this.list.benchTypeConf.rdfType;
      rt = lang.isArray(rt) ? rt[0] : rt;
      const info = localize(this.NLSBundles.eswoImportDialog, 'importInformation', {
        entityType: this.list.getName(),
        rdfType: rt,
      });
      domAttr.set(this.__importInformation, 'innerHTML', info);
    },
    footerButtonAction() {
      const val = this.entryType.getValue();
      const f = lang.hitch(this, this.importData);

      if (this.entryType.isFile()) {
        const inp = this.entryType.getFileInputElement();
        return defaults.get('entrystore').echoFile(inp, 'text').then(f);
      }
      return defaults.get('entrystore').loadViaProxy(val, 'application/rdf+xml').then(f);
    },
    importData(data) {
      const report = converters.detect(data);
      if (!report.error) {
        const entityURIs = this.detectEntities(report.graph);
        return all(array.map(entityURIs, lang.hitch(this, this.importEntity, report.graph)));
      }
      throw report.error;
    },
    detectEntities(graph) {
      let rt = this.list.benchTypeConf.rdfType;
      rt = lang.isArray(rt) ? rt[0] : rt;
      return array.map(graph.find(null, 'rdf:type', rt), stmt => stmt.getSubject());
    },
    importEntity(graph, entityURI) {
      const np = defaults.get('context').newLink(entityURI);
      const ngraph = utils.extract(graph, entityURI);
      this.mapEntityMetadata(ngraph, entityURI);
      np.setMetadata(ngraph);
      return np.commit().then((entry) => {
        this.list.addRowForEntry(entry);
      });
    },
    mapEntityMetadata(graph) {
      const map = this.list.benchTypeConf.importMap;
      if (map) {
        Object.keys(map).forEach((key) => {
          const stmts = graph.find(null, key, null);
          stmts.forEach((stmt) => {
            const o = stmt.getObject();
            delete o._statement;
            const oc = lang.clone(o);
            o._statement = stmt;

            graph.add(stmt.getSubject(), map[key], oc);
          });
        });
      }
    },
  }));
