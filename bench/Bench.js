/* eslint no-alert: "off" */
define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-attr',
  'dojo/dom-construct',
  'dojo/dom-class',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'entryscape-commons/create/typeIndex',
  'entryscape-commons/placeholder/Placeholder',
  'entryscape-commons/view/ViewMixin',
  './List',
  './ListAndContentView',
  'config',
  '../defaults',
  'dojo/text!./BenchTemplate.html',
  '../utils/entitytypes',
  'dojo/dom-style',
  'di18n/NLSMixin',
  'i18n!nls/eswoBench',
], (declare, lang, domAttr, domConstruct, domClass, _WidgetBase, _TemplatedMixin, typeIndex,
    Placeholder, ViewMixin, List, ListAndContentView, config, defaults, template,
    entitytypes, domStyle, NLSMixin) =>

  declare([_WidgetBase, _TemplatedMixin, NLSMixin.Dijit, ViewMixin], {
    bid: 'eswoBench',
    nlsBundles: ['eswoBench'],
    templateString: template,
    viewName: 'workbench__entities',
    __sideList: null,
    __list: null,
    __placeholder: null,
    configuredEntitytypes: [],

    updateBadge(conf) {
      if (!this.name2badge) {
        return;
      }
      if (conf != null && this.name2badge[conf.name]) {
        const cid = defaults.get('context').getId();
        domAttr.set(this.name2badge[conf.name], 'innerHTML', typeIndex.get(cid, conf));
      }
    },
    postCreate() {
      this.inherited(arguments);
      const es = defaults.get('entrystore');
      es.addAsyncListener((promise, callType) => {
        if (callType === 'createEntry') {
          promise.then((entry) => {
            const sm = defaults.get('siteManager');
            const view = sm.getCurrentView();
            if (view === this.viewName) {
              typeIndex.add(entry);
              const conf = typeIndex.getConf(entry);
              this.updateBadge(conf);
            }
          });
        }
      });
    },
    getEntityNameFromURI(entityURI) {
      const es = defaults.get('entrystore');
      if (entityURI.indexOf(es.getBaseURI()) === 0) {
        return entityURI.substr(es.getResourceURI('entitytypes', '').length);
      }
      return entityURI;
    },
    show(viewInfo) {
      const { params } = viewInfo;
      this.configuredEntitytypes = [];
      this.filteredConfEtypes = [];
      defaults.get('context').getEntry().then((entry) => {
        const ei = entry.getEntryInfo();
        const graph = ei.getGraph();
        const entitytypeStmts = graph.find(null, 'esterms:entityType');
        if (entitytypeStmts && entitytypeStmts.length > 0) {
          if (entitytypeStmts.length === 1) {
            this.renderSingleEntity(this.getEntityNameFromURI(entitytypeStmts[0].getValue()));
            return;
          }
          entitytypeStmts.forEach((entitytype) => {
            this.configuredEntitytypes.push(this.getEntityNameFromURI(entitytype.getValue()));
          }, this);
        } else {
          config.entitytypes.forEach((configEntitytype) => {
            this.configuredEntitytypes.push(configEntitytype.name);
          }, this);
        }
        this.filteredConfEtypes = entitytypes.filterEntitytypes(this.configuredEntitytypes);
        this.configuredEntitytypes = entitytypes.sortNames(this.filteredConfEtypes);
        if (params.entity && this.configuredEntitytypes.indexOf(params.entity) !== -1) {
          this.render(params.entity);
        } else if (this.configuredEntitytypes && this.configuredEntitytypes.length > 0) {
          this.render(this.configuredEntitytypes[0]);
        } else {
          // TODO replace this with an akgnowledge dialog
          alert('Config error, no types given for workbench.');
        }
      });
    },

    getViewLabel(view, params, callback) {
      const rdfutils = defaults.get('rdfutils');
      let context = defaults.get('context');
      if (context) {
        context = defaults.get('context').getEntry().then((contextEntry) => {
          callback(rdfutils.getLabel(contextEntry));
        });
      } else {
        callback('???');
      }
    },

    destroyEditors() {
      if (this.list) {
        this.list.destroy();
        delete this.list;
      }
      if (this.listAndEditor) {
        this.listAndEditor.destroy();
        delete this.listAndEditor;
      }
    },

    renderSingleEntity(singleEntity) {
      domStyle.set(this.__multipleEtypes, 'display', 'none');
      domStyle.set(this.__singleEtype, 'display', 'block');
      domAttr.set(this.__list, 'innerHTML', '');
      domAttr.set(this.__singleEtypeList, 'innerHTML', '');
      this.destroyEditors();
      const contextEntry = defaults.get('context').getEntry(true);
      const typeConf = typeIndex.getConfByName(singleEntity);
      this.createList(typeConf, contextEntry.canWriteResource(), this.__singleEtypeList);
    },

    createList(conf, isWrite, listNode) {
      domStyle.set(this.__placeholder, 'display', 'none');
      domStyle.set(this.__list, 'display', '');

      const newNode = domConstruct.create('div', null, listNode);
      if (conf.split) {
        this.listAndEditor = new ListAndContentView({
          benchTypeConf: conf,
          bench: this,
          mode: isWrite ? 'edit' : 'present',
        }, newNode);
        this.listAndEditor.show();
      } else {
        this.list = new List({
          benchTypeConf: conf,
          bench: this,
          mode: isWrite ? 'edit' : 'present',
          includeMassOperations: (config.workbench && config.workbench.includeMassOperations) ? true : false,
        }, newNode);
        this.list.show();
      }
    },

    render(selectedType) {
      const sm = defaults.get('siteManager');
      const view = sm.getUpcomingOrCurrentView();
      const uparams = sm.getUpcomingOrCurrentParams();
      domStyle.set(this.__multipleEtypes, 'display', 'block');
      domStyle.set(this.__singleEtype, 'display', 'none');
      domAttr.set(this.__list, 'innerHTML', '');
      domAttr.set(this.__sideList, 'innerHTML', '');
      domStyle.set(this.__list, 'display', 'none');
      domStyle.set(this.__placeholder, 'display', '');

      this.destroyEditors();

      this.name2badge = {};
      const cid = defaults.get('context').getId();
      const contextEntry = defaults.get('context').getEntry(true);
      this.configuredEntitytypes.forEach((cEntitytype) => {
        const typeConf = typeIndex.getConfByName(cEntitytype);
        const params = uparams;
        params.entity = typeConf.name;
        const node = domConstruct.create('li', {
          role: 'presentation',
          class: `${this.bid}__listItem`,
        }, this.__sideList);
        if (typeConf.name === selectedType) {
          this.createList(typeConf, contextEntry.canWriteResource(), this.__list);
          domClass.add(node, 'active');
        }
        const a = domConstruct.create('a', { href: sm.getViewPath(view, params) }, node);
        const title = defaults.get('localize')(typeConf.label);
        const badge = domConstruct.create('span', { class: 'badge pull-right' }, a);
        domConstruct.create('span', {
          innerHTML: title,
          class: 'eswoBench__entityName',
        }, a);
        this.name2badge[typeConf.name] = badge;
        typeIndex.update(cid, typeConf, badge);
      });
    },
    localeChange() {
      if (!this.placeholder) {
        this.placeholder = new Placeholder({}, domConstruct.create('div', null, this.__placeholder));
        this.placeholder.getText = () => this.NLSBundle0.selectEntitytypeMessage;
        this.placeholder.render();
      }
    },
  }));
