define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/aspect',
  'dojo/dom-construct',
  'dojo/dom-attr',
  'dojo/dom-style',
  'entryscape-commons/dialog/TitleDialog',
  'entryscape-commons/create/EntryType',
  'entryscape-commons/defaults',
  'dijit/_WidgetsInTemplateMixin',
  'entryscape-commons/list/common/ListDialogMixin',
  'dojo/text!./ReplaceDialogTemplate.html',
  'i18n!nls/eswoReplaceDialog',
], (declare, lang, aspect, domConstruct, domAttr, domStyle, TitleDialog, EntryType, defaults,
    _WidgetsInTemplateMixin, ListDialogMixin, template) =>

  declare([TitleDialog.ContentNLS, _WidgetsInTemplateMixin, ListDialogMixin], {
    templateString: template,
    bid: 'eswoReplaceDialog',
    maxWidth: 800,
    nlsBundles: ['eswoReplaceDialog'],
    nlsHeaderTitle: 'replaceFileHeader',
    nlsFooterButtonLabel: 'replaceFileFooterButton',
    postCreate() {
      // Add margin-left 1% somehow to be inline with rdforms.
      this.fileOrLink = new EntryType({
        valueChange: lang.hitch(this, (value) => {
          if (this.isFile) {
            this.dialog.unlockFooterButton();
            return;
          }
          if (value != null && (value !== this.oldValue)) {
            this.dialog.unlockFooterButton();
          } else {
            this.dialog.lockFooterButton();
          }
        }),
      }, domConstruct.create('div', null, this.__fileOrLink, 'first'));
      const f = lang.hitch(this, this.localeChange_fileOrLink);
      const lr = this.localeReady;
      aspect.after(this.fileOrLink, 'localeChange', () => {
        lr.then(f);
      });
      this.inherited(arguments);
    },
    localeChange() {
      if (this.isFile) {
        domStyle.set(this.__currentBlock, 'display', 'none');
        domAttr.set(this.dialog.titleNode, 'innerHTML', this.NLSBundle0.replaceFileHeader);
        domAttr.set(this.dialog.footerButtonLabelNode, 'innerHTML', this.NLSBundle0.replaceFileFooterButton);
      } else {
        domStyle.set(this.__currentBlock, 'display', '');
        domAttr.set(this.__currentLabel, 'innerHTML', this.NLSBundle0.currentLink);
        domAttr.set(this.dialog.titleNode, 'innerHTML', this.NLSBundle0.replaceLinkHeader);
        domAttr.set(this.dialog.footerButtonLabelNode, 'innerHTML', this.NLSBundle0.replaceLinkFooterButton);
      }
    },
    localeChange_fileOrLink() {
      if (this.isFile) {
        domAttr.set(this.fileOrLink.__fileLabel, 'innerHTML', this.NLSBundle0.newFile);
      } else {
        domAttr.set(this.fileOrLink.__linkLabel, 'innerHTML', this.NLSBundle0.newLink);
      }
    },
    open(params) {
      this.row = params.row;
      this.entry = params.row.entry;
      this.isLink = false;
      this.isFile = false;
      this.entry.setRefreshNeeded();
      this.entry.refresh().then(() => {
        if (this.entry.isLink()) {
          this.isLink = true;
        } else {
          this.isFile = true;
        }
        // check entry is link or file, display accordingly
        this.fileOrLink.show(this.isFile, this.isLink, false);
        this.localeChange();
        if (this.isLink) {
          this.oldValue = this.entry.getResourceURI();
        } else {
          this.oldValue = this.entry.getEntryInfo().getLabel();
        }
        domAttr.set(this.__currentValue, 'value', this.oldValue);
        this.dialog.show();
      });
      this.inherited(arguments);
    },
    footerButtonAction() {
      if (this.isLink) {
        const entryInfo = this.entry.getEntryInfo();
        const uri = this.fileOrLink.getValue();// check and replace uri
        entryInfo.setResourceURI(uri);
        return entryInfo.commit();
      }
      const inp = this.fileOrLink.getFileInputElement();
      return this.entry.getResource(true).putFile(inp);
    },
  }));
