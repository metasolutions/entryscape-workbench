define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-construct',
  'entryscape-commons/create/typeIndex',
  'entryscape-commons/list/common/CreateDialog',
  'entryscape-commons/create/EntryType',
  '../defaults',
], (declare, lang, domConstruct, typeIndex, CreateDialog, EntryType, defaults) =>

  declare([CreateDialog], {
    explicitNLS: true,
    postCreate() {
      // Add margin-left 1% somehow to be inline with rdforms.
      this.fileOrLink = new EntryType({
        valueChange: lang.hitch(this, (value) => {
          if (value != null) {
            this.unlockFooterButton();
          } else {
            this.lockFooterButton();
          }
        }),
      }, domConstruct.create('div', null, this.containerNode, 'first'));
      this.inherited(arguments);
    },

    open() {
      this.list.getView().clearSearch();
      const conf = this.list.benchTypeConf;
      // var conf = typeIndex.getConf();// check and get from list
      if (conf) {
        this.fileOrLink.showConfig(conf);
      } else {
        // What now?
      }
      // prototypeEntry need to be created before set acl properties, making unpublished
      this.inherited(arguments);
      if (conf.publishable && conf.publicFromStart !== true) {
        defaults.get('getGroupWithHomeContext')(this._newEntry.getContext())
          .then((groupEntry) => {
            const ei = this._newEntry.getEntryInfo();
            const acl = ei.getACL(true);
            acl.admin.push(groupEntry.getId());
            ei.setACL(acl);
          });
      }
    },

    doneAction(graph) {
      this._newEntry.setMetadata(graph);
      return this.fileOrLink.newEntry(this._newEntry).then((newEntry) => {
        this.list.addRowForEntry(newEntry);
      });
    },
  }));
