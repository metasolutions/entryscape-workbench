define([
  'dojo/_base/declare',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dijit/_WidgetsInTemplateMixin',
  'di18n/NLSMixin',
  'dojo/text!./ListAndContentViewTemplate.html',
  '../bench/List',
  'dojo/dom-construct',
  'entryscape-commons/contentview/ContentViewTabs',
  'dojo/dom-style',
  'entryscape-commons/placeholder/Placeholder',
  'entryscape-commons/create/typeIndex',
  '../defaults',
  'di18n/localize',
  'dojo/aspect',
  'dojo/_base/lang',
  'i18n!nls/eswoListAndContentView',
], (declare, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, NLSMixin, template, List,
    domConstruct, ContentViewTabs, domStyle, Placeholder, typeIndex, defaults,
    localize, aspect, lang) => {
  const ContentPlaceholder = declare([Placeholder], {
    nlsBundles: ['eswoListAndContentView'],

    getText() {
      return this.NLSBundle0.noContentViewerMessage;
    },

    render() {
      if (this.list.benchTypeConf && this.list.benchTypeConf.faClass) {
        this.missingImageClass = this.list.benchTypeConf.faClass;
      }
      this.inherited(arguments);
    },
  });

  return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, NLSMixin.Dijit], {
    templateString: template,
    bid: 'eswoListAndContentView',

    show() {
      this.list = new List({
        benchTypeConf: this.benchTypeConf,
        bench: this.bench,
        mode: this.mode,
        listAndContentViewer: this,
      }, domConstruct.create('div', null, this.__list));
      this.list.show();
      this.dPlaceholder = new ContentPlaceholder({
        search: false,
        list: this.list,
      }, domConstruct.create('div', null, this.__placeholder));
      this.dPlaceholder.render();
      aspect.after(this.list.getView(), 'updateListCount', lang.hitch(this, () => {
        if (this.list.getView().entryList.getSize() > 0) {
          domStyle.set(this.__placeholder, 'display', 'block');
        } else {
          domStyle.set(this.__placeholder, 'display', 'none');
        }
        this.clearContentView();
      }), true);

      this.inherited('show', arguments);
    },
    clearContentView() {
      if (this.list.getView().getSize() > 0) {
        domStyle.set(this.__placeholder, 'display', 'block');
      } else {
        domStyle.set(this.__placeholder, 'display', 'none');
      }
      domStyle.set(this.__contentEditor, 'display', 'none');
      if (this.contentViewer) {
        this.contentViewer.destroy();
      }
    },
    open(benchTypeConf, params) {
      this.clearContentView();
      domStyle.set(this.__placeholder, 'display', 'none');
      domStyle.set(this.__contentEditor, 'display', '');
      this.contentViewer = new ContentViewTabs({
        entityConf: benchTypeConf,
        entry: params.row.entry,
      }, domConstruct.create('div', null, this.__contentEditor));
    },
  });
});
