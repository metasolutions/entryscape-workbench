define({
  templates: [{
    type: 'group',
    nodetype: 'RESOURCE',
    id: 'tora:Image',
    constraints: {
      'http://www.w3.org/1999/02/22-rdf-syntax-ns#type': 'http://schema.org/ImageObject',
    },
    label: {
      en: 'Image',
      sv: 'Bild',
    },
    items: [{
      id: 'schema:caption',
    },
    {
      extends: 'schema:contentURL',
      nodetype: 'URI',
      type: 'text',
      styles: [
        'image',
      ],
      label: {
        en: 'Image',
        sv: 'Bild',
      },
    },
    {
      type: 'text',
      extends: 'dcterms:identifier',
      cardinality: {
        min: 0,
        pref: 1,
      },
    },
    {
      type: 'text',
      extends: 'dcterms:description',
      label: {
        en: 'Description',
        sv: 'Beskrivning',
      },
      cardinality: {
        min: 0,
        pref: 1,
      },
    },
    ],
  },
  {
    type: 'group',
    nodetype: 'RESOURCE',
    id: 'tora:digitizedsourcematerial',
    constraints: {
      'http://www.w3.org/1999/02/22-rdf-syntax-ns#type': 'https://data.riksarkivet.se/tora/schema/DigitizedSourceMaterial',
    },
    label: {
      en: 'Suecia metadata',
      sv: 'Suecia metadata',
    },
    items: [{
      type: 'text',
      extends: 'skos:prefLabel',
      cardinality: {
        min: 1,
      },
      label: {
        en: 'Name',
        sv: 'Namn',
      },
    },
    {
      type: 'text',
      extends: 'dc:creator',
      cardinality: {
        min: 0,
        pref: 1,
      },
      label: {
        en: 'Creator',
        sv: 'Upphovsman',
      },
    },
    {
      type: 'text',
      extends: 'dcterms:identifier',
      cardinality: {
        min: 1,
        pref: 1,
      },
      label: {
        en: 'Libris reference',
        sv: 'Libris-referens',
      },
    },
    {
      type: 'text',
      extends: 'dcterms:references',
      cardinality: {
        min: 1,
        pref: 1,
      },
      label: {
        en: 'TORA reference',
        sv: 'TORA-referens',
      },
    },
    {
      type: 'text',
      extends: 'dc:type',
      cardinality: {
        min: 0,
        pref: 1,
      },
      label: {
        en: 'Classification',
        sv: 'Ã„mnesord',
      },
    },
    {
      type: 'text',
      extends: 'dcterms:description',
      label: {
        en: 'Year',
        sv: 'Ã…rtal',
      },
      cardinality: {
        min: 0,
      },
    },
    {
      type: 'choice',
      nodetype: 'URI',
      extends: 'dcterms:publisher',
      constraints: {
        'http://www.w3.org/1999/02/22-rdf-syntax-ns#type': 'https://data.riksarkivet.se/tora/schema/ToraPartner',
      },
      cardinality: {
        min: 1,
      },
      label: {
        sv: 'TillgÃ¤ngliggjord av',
      },
    },
    {
      type: 'choice',
      nodetype: 'URI',
      extends: 'schema:image',
      constraints: {
        'http://www.w3.org/1999/02/22-rdf-syntax-ns#type': 'http://schema.org/ImageObject',
      },
      cardinality: {
        min: 0,
        pref: 1,
      },
    },
    ],
  },
  {
    type: 'text',
    id: 'tora:province',
    property: 'https://data.riksarkivet.se/tora/schema/Province',
    nodetype: 'LITERAL',
    label: {
      en: 'Province',
      sv: 'Landskap',
    },
    cardinality: {
      min: 0,
      pref: 1,
    },
  },
  {
    type: 'text',
    id: 'tora:hundered',
    property: 'https://data.riksarkivet.se/tora/schema/Hundered',
    nodetype: 'LITERAL',
    label: {
      en: 'Hundered',
      sv: 'HÃ¤rad',
    },
    cardinality: {
      min: 0,
      pref: 1,
    },
  },
  {
    type: 'text',
    id: 'tora:historicalcadastralparish',
    property: 'https://data.riksarkivet.se/tora/schema/HistoricalCadastralParish',
    nodetype: 'LITERAL',
    label: {
      en: 'Cadastral Parish',
      sv: 'Jordebokssocken',
    },
    cardinality: {
      min: 1,
    },
  },
  {
    type: 'text',
    id: 'tora:namingcomment',
    property: 'https://data.riksarkivet.se/tora/schema/NamingComment',
    nodetype: 'LITERAL',
    label: {
      en: 'Naming comment',
      sv: 'Namnkommentar',
    },
    cardinality: {
      min: 0,
      pref: 1,
    },
  },
  {
    type: 'choice',
    id: 'tora:coordinate_precision',
    property: 'https://data.riksarkivet.se/tora/schema/CoordinatePrecision',
    nodetype: 'LITERAL',
    label: {
      en: 'Coordinate precision',
      sv: 'Koordinatprecision',
    },
    cardinality: {
      min: 1,
      pref: 1,
    },
  },
  {
    type: 'text',
    id: 'tora:coordinate_comment',
    property: 'https://data.riksarkivet.se/tora/schema/CoordinateComment',
    nodetype: 'LITERAL',
    label: {
      en: 'Coordinate comment',
      sv: 'Koordinatkommentar',
    },
    cardinality: {
      min: 0,
      pref: 1,
    },
  },
  {
    type: 'text',
    id: 'tora:churchparishpolygon',
    property: 'https://data.riksarkivet.se/tora/schema/ChurchParishPolygon',
    nodetype: 'LITERAL',
    label: {
      en: 'Church parish polygon',
      sv: 'FÃ¶rsamlingspolygon',
    },
    cardinality: {
      min: 0,
      pref: 1,
    },
  },
  {
    type: 'text',
    id: 'tora:coordinate_source1',
    property: 'https://data.riksarkivet.se/tora/schema/CoordinateSource1',
    nodetype: 'LITERAL',
    label: {
      en: 'Coordinate source 1',
      sv: 'KoordinatkÃ¤lla 1',
    },
    cardinality: {
      min: 0,
      pref: 1,
    },
  },
  {
    type: 'text',
    id: 'tora:coordinate_source2',
    property: 'https://data.riksarkivet.se/tora/schema/CoordinateSource2',
    nodetype: 'LITERAL',
    label: {
      en: 'Coordinate source 2',
      sv: 'KoordinatkÃ¤lla 2',
    },
    cardinality: {
      min: 0,
      pref: 1,
    },
  },
  {
    type: 'text',
    id: 'geo:lat',
    property: 'http://www.w3.org/2003/01/geo/wgs84_pos/lat',
    nodetype: 'LITERAL',
    label: {
      en: 'WGS84 latitude ',
      sv: 'WGS84 latitud',
    },
    cardinality: {
      min: 0,
      pref: 1,
    },
  },
  {
    type: 'text',
    id: 'geo:long',
    property: 'http://www.w3.org/2003/01/geo/wgs84_pos/long',
    nodetype: 'LITERAL',
    label: {
      en: 'WGS84 Longitude',
      sv: 'WGS84 longitud',
    },
    cardinality: {
      min: 0,
      pref: 1,
    },
  },
  {
    type: 'text',
    id: 'tora:sweref99_y',
    property: 'https://data.riksarkivet.se/tora/schema/sweref99_y',
    nodetype: 'LITERAL',
    label: {
      en: 'Sweref99 Y',
      sv: 'Sweref99 Y',
    },
    cardinality: {
      min: 0,
      pref: 1,
    },
  },
  {
    type: 'text',
    id: 'tora:rt90_x',
    property: 'https://data.riksarkivet.se/tora/schema/rt90_x',
    nodetype: 'LITERAL',
    label: {
      en: 'RT90 X',
      sv: 'RT90 X',
    },
    cardinality: {
      min: 0,
      pref: 1,
    },
  },
  {
    type: 'text',
    id: 'tora:rt90_y',
    property: 'https://data.riksarkivet.se/tora/schema/rt90_y',
    nodetype: 'LITERAL',
    label: {
      en: 'RT90 Y',
      sv: 'RT90 Y',
    },
    cardinality: {
      min: 0,
      pref: 1,
    },
  },
  {
    type: 'text',
    id: 'tora:sweref99_x',
    property: 'https://data.riksarkivet.se/tora/schema/sweref99_x',
    nodetype: 'LITERAL',
    label: {
      en: 'Sweref99 X',
      sv: 'Sweref99 X',
    },
    cardinality: {
      min: 0,
      pref: 1,
    },
  },
  {
    type: 'group',
    nodetype: 'RESOURCE',
    id: 'tora:torapartner',
    constraints: {
      'http://www.w3.org/1999/02/22-rdf-syntax-ns#type': 'https://data.riksarkivet.se/tora/schema/ToraPartner',
    },
    label: {
      en: 'Coordinate creator',
      sv: 'Koordinatupphovsman',
    },
    items: [{
      type: 'text',
      extends: 'dcterms:title',
      cardinality: {
        min: 1,
      },
    },
    {
      type: 'text',
      extends: 'skos:prefLabel',
      label: {
        en: 'Project',
        sv: 'Projekt',
      },
      cardinality: {
        min: 1,
      },
    },
    {
      type: 'text',
      extends: 'dcterms:subject',
      cardinality: {
        min: 0,
        pref: 1,
      },
      label: {
        en: 'URL',
        sv: 'Webbsida',
      },
    },
    {
      typ: 'text',
      extends: 'dcterms:description',
      cardinality: {
        min: 0,
        pref: 1,
      },
    },
    ],
  },
  {
    type: 'group',
    nodetype: 'RESOURCE',
    id: 'tora:coordinateprecision',
    constraints: {
      'http://www.w3.org/1999/02/22-rdf-syntax-ns#type': 'https://data.riksarkivet.se/tora/schema/CoordinatePrecisionValue',
    },
    label: {
      en: 'Coordinate Precision',
      sv: 'Koordinatprecision',
    },
    items: [{
      type: 'text',
      extends: 'dcterms:title',
      cardinality: {
        min: 1,
      },
    },
    {
      type: 'text',
      extends: 'skos:prefLabel',
      label: {
        en: 'Precision',
        sv: 'Precision',
      },
      cardinality: {
        min: 1,
      },
    },
    {
      typ: 'text',
      extends: 'dcterms:description',
      cardinality: {
        min: 0,
        pref: 1,
      },
    },
    ],
  },
  {
    type: 'group',
    nodetype: 'RESOURCE',
    id: 'tora:alternatename',
    constraints: {
      'http://www.w3.org/1999/02/22-rdf-syntax-ns#type': 'https://data.riksarkivet.se/tora/schema/AlternateName',
    },
    label: {
      en: 'Alternate Name',
      sv: 'Alternativt namn',
    },
    items: [{
      type: 'text',
      extends: 'dcterms:identifier',
      cardinality: {
        min: 1,
      },
    },
    {
      type: 'text',
      extends: 'dcterms:title',
      label: {
        en: 'Name',
        sv: 'namn',
      },
      cardinality: {
        min: 1,
      },
    },
    {
      type: 'text',
      extends: 'skos:altLabel',
      label: {
        en: 'Alternate Name',
        sv: 'Alternativt namn',
      },
      cardinality: {

        min: 0,
        pref: 1,
      },
    },
    {
      type: 'choice',
      nodetype: 'URI',
      id: 'tora:torapartner',
      extends: 'dcterms:creator',
      constraints: {
        'http://www.w3.org/1999/02/22-rdf-syntax-ns#type': 'https://data.riksarkivet.se/tora/schema/ToraPartner',
      },
      cardinality: {
        min: 1,
      },
      label: {
        en: 'Naming authority',
        sv: 'Namngivande auktoritet',
      },
    },
    {
      type: 'choice',
      nodetype: 'URI',
      id: 'tora:namingprinciple',
      extends: 'dcterms:conformsTo',
      constraints: {
        'http://www.w3.org/1999/02/22-rdf-syntax-ns#type': 'https://data.riksarkivet.se/tora/schema/NamingPrinciple',
      },
      cardinality: {
        min: 0,
        pref: 1,
      },
      label: {
        en: 'Alternative Naming Principle',
        sv: 'Alternativnamnets namnprincip',
      },
    },
    {
      type: 'text',
      extends: 'dcterms:description',
      cardinality: {
        min: 0,
        pref: 1,
      },
    },
    ],
  },
  {
    type: 'group',
    nodetype: 'RESOURCE',
    id: 'tora:namingprinciple',
    constraints: {
      'http://www.w3.org/1999/02/22-rdf-syntax-ns#type': 'https://data.riksarkivet.se/tora/schema/NamingPrinciple',
    },
    label: {
      en: 'Naming principle',
      sv: 'Namnprincip',
    },
    items: [{
      type: 'text',
      extends: 'dcterms:title',
      cardinality: {
        min: 1,
      },
    },
    {
      type: 'text',
      extends: 'skos:prefLabel',
      label: {
        en: 'Name',
        sv: 'Namn',
      },
      cardinality: {
        min: 1,
      },
    },
    {
      type: 'text',
      extends: 'dcterms:description',
      cardinality: {
        min: 0,
        pref: 1,
      },
    },
    ],
  },
  {
    type: 'group',
    nodetype: 'RESOURCE',
    id: 'tora:historicaladministrativedivision',
    constraints: {
      'http://www.w3.org/1999/02/22-rdf-syntax-ns#type': 'https://data.riksarkivet.se/tora/schema/HistoricalAdministrativeDivision',
    },
    label: {
      en: 'Historical administrative division',
      sv: 'Historisk administrativ indelning',
    },
    items: [{
      type: 'choice',
      nodetype: 'URI',
      id: 'tora:torapartner',
      extends: 'dcterms:creator',
      constraints: {
        'http://www.w3.org/1999/02/22-rdf-syntax-ns#type': 'https://data.riksarkivet.se/tora/schema/ToraPartner',
      },
      cardinality: {
        min: 1,
      },
      label: {
        en: 'Historical authority',
        sv: 'Historisk auktoritet',
      },
    },
    {
      type: 'text',
      extends: 'dcterms:identifier',
      cardinality: {
        min: 1,
      },
    },
    {
      type: 'choice',
      extends: 'dcterms:references',
      cardinality: {
        min: 0,
        pref: 1,
      },
    },
    {
      type: 'text',
      extends: 'dcterms:bibliographicCitation',
      label: {
        en: 'Source reference',
        sv: 'KÃ¤llreferens',
      },
      cardinality: {
        min: 0,
        pref: 1,
      },
    },
    {
      type: 'text',
      extends: 'dcterms:source',
      label: {
        en: 'Source',
        sv: 'KÃ¤lla',
      },
      cardinality: {
        min: 0,
        pref: 1,
      },
    },
    {
      type: 'text',
      extends: 'dcterms:title',
      cardinality: {
        min: 0,
        pref: 1,
      },
      label: {
        en: 'Name of Settlement Unit',
        sv: 'Namn pÃ¥ bebyggelseenhet',
      },
    },
    {
      id: 'tora:province',
    },
    {
      id: 'tora:hundered',
    },
    {
      id: 'tora:historicalcadastralparish',
    },
    ],
  },
  {
    type: 'group',
    nodetype: 'RESOURCE',
    id: 'tora:historicalevent',
    constraints: {
      'http://www.w3.org/1999/02/22-rdf-syntax-ns#type': 'https://data.riksarkivet.se/tora/schema/HistoricalEvent',
    },
    label: {
      en: 'Historical Event',
      sv: 'Historisk hÃ¤ndelse',
    },
    items: [{
      type: 'choice',
      nodetype: 'URI',
      id: 'tora:torapartner',
      extends: 'dcterms:creator',
      constraints: {
        'http://www.w3.org/1999/02/22-rdf-syntax-ns#type': 'https://data.riksarkivet.se/tora/schema/ToraPartner',
      },
      cardinality: {
        min: 1,
      },
      label: {
        en: 'Historical authority',
        sv: 'Historisk auktoritet',
      },
    },
    {
      type: 'text',
      extends: 'dcterms:identifier',
      cardinality: {
        min: 0,
        pref: 1,
      },
    },
    {
      type: 'choice',
      extends: 'dcterms:references',
      cardinality: {
        min: 0,
        pref: 1,
      },
    },
    {
      type: 'text',
      extends: 'dcterms:source',
      label: {
        en: 'Source',
        sv: 'KÃ¤lla',
      },
      cardinality: {
        min: 0,
        pref: 1,
      },
    },
    {
      type: 'text',
      extends: 'dcterms:bibliographicCitation',
      label: {
        en: 'Reference to Source',
        sv: 'KÃ¤llreferens',
      },
      cardinality: {
        min: 0,
        pref: 1,
      },
    },
    {
      type: 'text',
      extends: 'skos:prefLabel',
      label: {
        en: 'Name of Settlement Unit',
        sv: 'Namn pÃ¥ bebyggelseenhet',
      },
      cardinality: {
        min: 1,
      },
    },
    {
      type: 'text',
      extends: 'dcterms:date',
      label: {
        en: 'Year',
        sv: 'Ã…rtal',
      },
      cardinality: {
        min: 1,
      },
    },
    {
      type: 'text',
      extends: 'dcterms:description',
      label: {
        en: 'Event',
        sv: 'HÃ¤ndelse',
      },
      cardinality: {
        min: 0,
        pref: 1,
      },
    },
    ],
  },
  {
    type: 'group',
    nodetype: 'RESOURCE',
    id: 'tora:HistoricalSettlementUnit',
    constraints: {
      'http://www.w3.org/1999/02/22-rdf-syntax-ns#type': 'https://data.riksarkivet.se/tora/schema/HistoricalSettlementUnit',
    },
    label: {
      en: 'Historical settlement unit (TORA)',
      sv: 'Historisk bebyggelseenhet (TORA)',
    },
    items: [{
      type: 'text',
      extends: 'dcterms:identifier',
      cardinality: {
        min: 1,
      },
    },
    {
      type: 'text',
      extends: 'skos:prefLabel',
      cardinality: {
        min: 1,
      },
      label: {
        en: 'Name',
        sv: 'Namn',
      },
    },
    {
      type: 'choice',
      nodetype: 'URI',
      id: 'tora:namingprinciple',
      extends: 'dcterms:conformsTo',
      constraints: {
        'http://www.w3.org/1999/02/22-rdf-syntax-ns#type': 'https://data.riksarkivet.se/tora/schema/NamingPrinciple',
      },
      cardinality: {
        min: 0,
        pref: 1,
      },
      label: {
        en: 'Naming Principle',
        sv: 'Namnprincip',
      },
    },
    {
      id: 'tora:namingcomment',
    },
    {
      type: 'choice',
      nodetype: 'URI',
      id: 'tora:alternatename',
      extends: 'rdfs:seeAlso',
      constraints: {
        'http://www.w3.org/1999/02/22-rdf-syntax-ns#type': 'https://data.riksarkivet.se/tora/schema/AlternateName',
      },
      cardinality: {
        min: 0,
        pref: 1,
      },
      label: {
        en: 'Alternate name',
        sv: 'Alternativt namn',
      },
    },
    {
      type: 'choice',
      nodetype: 'URI',
      extends: 'dcterms:relation',
      id: 'tora:historicaladministrativedivision',
      constraints: {
        'http://www.w3.org/1999/02/22-rdf-syntax-ns#type': 'https://data.riksarkivet.se/tora/schema/HistoricalAdministrativeDivision',
      },
      cardinality: {
        min: 0,
        pref: 1,
      },
      label: {
        en: 'Historical administrative division',
        sv: 'Historisk administrativ indelning',
      },
    },
    {
      type: 'choice',
      nodetype: 'URI',
      id: 'tora:historicalevent',
      extends: 'dcterms:references',
      constraints: {
        'http://www.w3.org/1999/02/22-rdf-syntax-ns#type': 'https://data.riksarkivet.se/tora/schema/HistoricalEvent',
      },
      cardinality: {
        min: 0,
        pref: 1,
      },
      label: {
        en: 'Historical Event',
        sv: 'Historisk hÃ¤ndelse',
      },
    },
    {
      type: 'choice',
      nodetype: 'URI',
      id: 'tora:torapartner',
      extends: 'dcterms:creator',
      constraints: {
        'http://www.w3.org/1999/02/22-rdf-syntax-ns#type': 'https://data.riksarkivet.se/tora/schema/ToraPartner',
      },
      cardinality: {
        min: 1,
      },
      label: {
        en: 'Coordinate Plotting',
        sv: 'KoordinatsÃ¤ttning',
      },
    },
    {
      type: 'choice',
      nodetype: 'URI',
      id: 'tora:coordinateprecision',
      extends: 'dcterms:coverage',
      constraints: {
        'http://www.w3.org/1999/02/22-rdf-syntax-ns#type': 'https://data.riksarkivet.se/tora/schema/CoordinatePrecision',
      },
      cardinality: {
        min: 1,
      },
      label: {
        en: 'Coordinate Precision',
        sv: 'Koordinatprecision',
      },
    },
    {
      id: 'tora:coordinate_comment',
    },
    {
      id: 'tora:coordinate_source1',
    },
    {
      id: 'tora:coordinate_source2',
    },
    {
      id: 'geo:lat',
    },
    {
      id: 'geo:long',
    },
    {
      id: 'tora:rt90_x',
    },
    {
      id: 'tora:rt90_y',
    },
    {
      id: 'tora:sweref99_x',
    },
    {
      id: 'tora:sweref99_y',
    },
    {
      id: 'tora:churchparishpolygon',
    },
    ],
  },
  ],
});
