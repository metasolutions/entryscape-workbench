define([
  'dojo/_base/declare',
  'dojo/dom-attr',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dijit/_WidgetsInTemplateMixin',
  'di18n/NLSMixin',
  '../defaults',
  'rdforms/view/renderingContext',
  'entryscape-commons/dialog/HeaderDialog', // In template
  'dojo/text!./CreateDialogTemplate.html',
  'entryscape-commons/dialog/TitleDialog',
  'entryscape-commons/list/common/ListDialogMixin',
  'i18n!nls/eswoSpaces',
], (declare, domAttr, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin,
    NLSMixin, defaults, renderingContext, HeaderDialog, template, TitleDialog, ListDialogMixin) =>

  declare([TitleDialog.ContentNLS, _WidgetsInTemplateMixin, ListDialogMixin, NLSMixin.Dijit], {
    templateString: template,
    maxWidth: 800,
    nlsBundles: ['eswoSpaces'],
    nlsHeaderTitle: 'createWorkspaceHeader',
    nlsFooterButtonLabel: 'createWorkspaceButton',

    postCreate() {
      this.inherited(arguments);
    },
    open() {
      this.list.getView().clearSearch();
      domAttr.set(this.workspaceName, 'value', '');
      domAttr.set(this.workspaceDesc, 'value', '');
      this.dialog.show();
    },
    footerButtonAction() {
      let group;
      let hc;
      const name = domAttr.get(this.workspaceName, 'value');
      const desc = domAttr.get(this.workspaceDesc, 'value');
      const store = defaults.get('entrystore');
      if (name === '' || desc === '') {
        return this.NLSBundles.eswoSpaces.insufficientInfoToCreateWorkspace;
      }
      let context;
      let centry;
      return store.createGroupAndContext()
        .then((entry) => {
          group = entry;
          hc = entry.getResource(true).getHomeContext();
          context = store.getContextById(hc);
          if (!defaults.get('hasAdminRights')) {
            this.list.entryList.setGroupIdForContext(context.getId(), group.getId());
          }
          return context.getEntry();
        })
        .then((contextEntry) => {
          centry = contextEntry;
          const l = renderingContext.getDefaultLanguage();
          const md = contextEntry.getMetadata();
          md.addL(contextEntry.getResourceURI(), 'dcterms:title', name, l);
          md.addL(contextEntry.getResourceURI(), 'dcterms:description', desc, l);
          return contextEntry.commitMetadata();
        })
        .then((ctxEntry) => {
          const hcEntryInfo = ctxEntry.getEntryInfo();
          hcEntryInfo.getGraph().add(ctxEntry.getResourceURI(), 'rdf:type', 'esterms:WorkbenchContext');
          // TODO remove when entrystore is changed so groups have read access to
          // homecontext metadata by default.
          // Start fix with missing metadata rights on context for group
          const acl = hcEntryInfo.getACL(true);
          acl.mread.push(group.getId());
          hcEntryInfo.setACL(acl);
          // End fix
          return hcEntryInfo.commit().then(() => {
            const row = this.list.getView().addRowForEntry(centry);
            this.list.rowMetadataUpdated(row);
            const userEntry = defaults.get('userEntry');
            userEntry.setRefreshNeeded();
            userEntry.refresh();
          });
        }, (err) => {
          // dialogs.acknowledge(err);
          throw err;
        });
    },
  }));
