define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-prop',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dijit/_WidgetsInTemplateMixin',
  'di18n/NLSMixin',
  '../defaults',
  '../utils/entitytypes',
  'entryscape-commons/dialog/TitleDialog',
  'entryscape-commons/list/common/ListDialogMixin',
  'dojo/dom-class',
  'dojo/dom-style',
  'dojo/dom-construct',
  'dojo/_base/array',
  'config',
  'jquery',
  'dojo/on',
  'di18n/localize',
  'i18n!nls/eswoConfigureEntityTypes',
], (declare, lang, domProp, _WidgetBase, _TemplatedMixin,
    _WidgetsInTemplateMixin, NLSMixin, defaults, entitytypes, TitleDialog,
    ListDialogMixin, domClass, domStyle, domConstruct, array, config, jquery, on) =>
  declare(
    [TitleDialog.ContentNLS, _WidgetsInTemplateMixin, ListDialogMixin, NLSMixin.Dijit], {
      bid: 'eswoConfigureEntityTypes',
      templateString: '<div data-dojo-attach-point="__entitytypeList" class="entityTypeList"></div>',
      maxWidth: 800,
      nlsBundles: ['eswoConfigureEntityTypes'],
      nlsHeaderTitle: 'entityTypesHeader',
      nlsFooterButtonLabel: 'entityTypesFooter',
      items: null,
      configuredEntitytypes: [],
      oldCEntitytypes: [],

      open(params) {
        this.render();
        this.entry = params.row.entry;
        this.update();
        this.dialog.closeErrorMessage();
        this.dialog.lockFooterButton();
        this.dialog.show();
      },
      footerButtonAction() {
        const entryInfo = this.entry.getEntryInfo();
        const graph = entryInfo.getGraph();
        graph.findAndRemove(this.entry.getResourceURI(), 'esterms:entityType');
        if (this.entitytypeCheckboxes.length !== this.configuredEntitytypes.length) {
          array.forEach(this.configuredEntitytypes, (cEntitytype) => {
            if (cEntitytype.indexOf('http') === 0) {
              graph.add(this.entry.getResourceURI(), 'esterms:entityType', cEntitytype);
            } else {
              graph.addL(this.entry.getResourceURI(), 'esterms:entityType', cEntitytype);
            }
          }, this);
        }
        return entryInfo.commit();
      },
      update() {
        this.configuredEntitytypes = [];
        this.oldCEntitytypes = [];
        const entryInfo = this.entry.getEntryInfo();
        const graph = entryInfo.getGraph();
        const ets = graph.find(this.entry.getResourceURI(), 'esterms:entityType');
        if (ets && ets.length > 0) {
          this.entitytypeCheckboxes.forEach((entitytypeCheckbox) => {
            domProp.set(entitytypeCheckbox.entityEl, 'checked', false);
          });
          ets.forEach((entitytype) => {
            const et = entitytype.getValue();
            this.configuredEntitytypes.push(et);
            this.oldCEntitytypes.push(et);
            this.check(et);
          });
        } else {
          this.entitytypeCheckboxes.forEach((entitytypeCheckbox) => {
            this.configuredEntitytypes.push(entitytypeCheckbox.entitytypeURI);
            this.oldCEntitytypes.push(entitytypeCheckbox.entitytypeURI);
            domProp.set(entitytypeCheckbox.entityEl, 'checked', true);
          });
        }
      },
      getEntityURI(entitytypename) {
        const es = defaults.get('entrystore');
        if (entitytypename.indexOf(es.getBaseURI()) === -1) {
          return es.getResourceURI('entitytypes', entitytypename);
        }
        return entitytypename;
      },
      check(etURI) {
        this.entitytypeCheckboxes.forEach((entitytypeCheckbox) => {
          if (entitytypeCheckbox.entitytypeURI === etURI) {
            domProp.set(entitytypeCheckbox.entityEl, 'checked', true);
          }
        });
      },
      render() {
        this.entitytypeCheckboxes = [];
        domConstruct.empty(this.__entitytypeList);
        const filteredEtypes = entitytypes.filterEntitypeConfigurations(config.entitytypes);
        const sortedEntitytypes = entitytypes.sort(filteredEtypes);
        sortedEntitytypes.forEach((entitytype) => {
          const divPanel = domConstruct.create('div', { class: 'togglebutton' }, this.__entitytypeList);
          const label = domConstruct.create('label', null, divPanel);
          const input = domConstruct.create('input', { type: 'checkbox' }, label);
          const task = domConstruct.create('span', {
            style: 'color: #333;',
            innerHTML: defaults.get('localize')(entitytype.label),
          }, label);
          const etURI = this.getEntityURI(entitytype.name);
          this.entitytypeCheckboxes.push({
            entityEl: input,
            entityNameEl: task,
            entitytypeURI: etURI,
          });
          const updateConfiguredEntitytypes = lang.hitch(this, this.setInModel, etURI);
          on(input, 'click', updateConfiguredEntitytypes);
        });
      },

      setInModel(etURI, event) {
        this.dialog.closeErrorMessage();
        const target = event.target || event.srcElement;
        if (target.checked) {
          this.configuredEntitytypes.push(etURI);
        } else {
          this.configuredEntitytypes.splice(this.configuredEntitytypes.indexOf(etURI), 1);
        }

        if (JSON.stringify(this.oldCEntitytypes) === JSON.stringify(this.configuredEntitytypes)) {
          this.dialog.lockFooterButton();
        } else {
          if (this.configuredEntitytypes.length === 0) {
            // show error message
            this.dialog.showErrorMessage(this.NLSBundle0.errorMessage);
            this.dialog.lockFooterButton();
            return;
          }
          this.dialog.unlockFooterButton();
        }
      },
    }));
