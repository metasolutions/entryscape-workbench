/* eslint quotes: ["error", "double"] */
/* eslint no-template-curly-in-string: "off" */
define({
  root: {
    entityTypesHeader: "Choose entity types for this project",
    entityTypesFooter: "Save",
    errorMessage: "The project must be configured with at least one entity type.",
  },
  sv: true,
  de: true,
});
