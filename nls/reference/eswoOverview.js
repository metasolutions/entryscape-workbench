/* eslint quotes: ["error", "double"] */
/* eslint no-template-curly-in-string: "off" */
define({
  root: {
    lastUpdatedLabel: "Updated",
    createdLabel: "Created",
    configuredEntitiesLabel: "Configured entity types",
    collectionLabel: "Collections",
  },
  sv: true,
  de: true,
});
