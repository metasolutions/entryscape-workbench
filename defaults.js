define([
  'entryscape-commons/defaults',
  'entryscape-commons/rdforms/linkBehaviour',
], (defaults) => {
  const ns = defaults.get('namespaces');
  ns.add('esterms', 'http://entryscape.com/terms/');
  return defaults;
});
