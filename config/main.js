define([
  'entryscape-commons/merge',
  'entryscape-workbench/config/workbenchConfig',
], (merge, workbenchConfig) => merge({
  theme: {
    appName: 'EntryScape Workbench',
    oneRowNavbar: true,
    localTheme: false,
  },
  locale: {
    fallback: 'en',
    supported: [
      { lang: 'de', flag: 'de', label: 'Deutsch', labelEn: 'German', shortDatePattern: 'dd. MMM' },
      { lang: 'en', flag: 'gb', label: 'English', labelEn: 'English', shortDatePattern: 'MMM dd' },
      { lang: 'sv', flag: 'se', label: 'Svenska', labelEn: 'Swedish', shortDatePattern: 'dd MMM' },
    ],
  },
  site: {
    siteClass: 'spa/Site',
    controlClass: 'entryscape-commons/nav/Layout',
    startView: 'workbenchstart',
  },
}, workbenchConfig, __entryscape_config));
