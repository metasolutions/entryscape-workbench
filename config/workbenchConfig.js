define({
  itemstore: {
    bundles: [
      'templates/dcterms/dcterms',
      'templates/foaf/foaf',
      'templates/vcard/vcard',
      'templates/skos/skos',
      'templates/entryscape/esc',
    ],
    choosers: [
      'entryscape-commons/rdforms/EntryChooser',
      'entryscape-commons/rdforms/SkosChooser',
    ],
  },
  site: {
    modules: {
      workbench: {
        productName: 'Workbench',
        faClass: 'table',
        sidebar: true,
        startView: 'workbench__list',
      },
    },
    views: {
      workbench__list: {
        class: 'entryscape-workbench/space/List',
        title: { en: 'Projects', sv: 'Projekt', de: 'Projekte' },
        faClass: 'building',
        route: '/workbench',
        module: 'workbench',
      },
      workbench: {
        class: 'entryscape-commons/gce/Cards',
        labelCrumb: true,
        route: '/workbench/:context',
        module: 'workbench',
        parent: 'workbench__list',
      },
      workbench__overview: {
        class: 'entryscape-workbench/overview/Overview',
        faClass: 'eye',
        title: { en: 'Overview', sv: 'Översikt', de: 'Überblick' },
        route: '/workbench/:context/overview',
        parent: 'workbench',
        module: 'workbench',
      },
      workbench__entities: {
        class: 'entryscape-workbench/bench/Bench',
        faClass: 'cube',
        title: { en: 'Entities', sv: 'Entiteter', de: 'Objekte' },
        route: '/workbench/:context/entitytype/?entity',
        parent: 'workbench',
        module: 'workbench',
      },
      //workbench__collections: {
      //  class: 'entryscape-workbench/collection/Collection',
      //  faClass: 'cubes',
      //  title: { en: 'Collections', sv: 'Samlingar', de: 'Samm&shy;lun&shy;gen' },
      //  route: '/workbench/:context/collections',
      //  parent: 'workbench',
      //  module: 'workbench',
      //},
    },
  },
});
